package id.co.imastudio.ojekonlinecourse.InitRetrofit;

import id.co.imastudio.ojekonlinecourse.Response.ResponseDaftar;
import id.co.imastudio.ojekonlinecourse.Response.ResponseHistory;
import id.co.imastudio.ojekonlinecourse.Response.ResponseInsertBooking;
import id.co.imastudio.ojekonlinecourse.Response.ResponseLogin;
import id.co.imastudio.ojekonlinecourse.Response.ResponseRoute;
import id.co.imastudio.ojekonlinecourse.Response.ResponseTracking;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService{

    @FormUrlEncoded
    @POST("daftar")
    Call<ResponseDaftar> request_daftar(
            @Field("nama") String name,
            @Field("email") String email,
            @Field("password") String pass,
            @Field("phone") String phone

    );
    @FormUrlEncoded
    @POST("cancel_booking")
    Call<ResponseDaftar> request_cancel(
            @Field("id_booking") String idbooking,
            @Field("f_token") String token,
            @Field("f_device") String device


    );
    @FormUrlEncoded
    @POST("check_booking")
    Call<ResponseHistory> request_check(
            @Field("id_booking") String idbooking,
            @Field("f_token") String token,
            @Field("f_device") String device


    );

@FormUrlEncoded
    @POST("login")
    Call<ResponseLogin> request_login(
            @Field("device") String device,
            @Field("f_email") String email,
            @Field("f_password") String pass


    );


    @GET("json")
    Call<ResponseRoute> request_route(
            @Query("origin") String orgin,
            @Query("destination") String desti,
            @Query("key") String key



    );

   /*$idUser = $this->input->post('f_idUser');
    $latAwal = $this->input->post('f_latAwal');
    $lngAwal = $this->input->post('f_lngAwal');
    $awal = $this->input->post('f_awal');
    $latAkhir = $this->input->post('f_latAkhir');
    $lngAkhir = $this->input->post('f_lngAkhir');
    $akhir = $this->input->post('f_akhir');
    $alamat = $this->input->post('f_alamat');
    $jarak = $this->input->post('f_jarak');
    $tarifUser =$this->input->post('f_tarif');*/


    @FormUrlEncoded
    @POST("insert_booking")
    Call<ResponseInsertBooking> request_insertbooking(
            @Field("f_token") String token,
            @Field("f_device") String device,
            @Field("f_idUser") String iduser,
            @Field("f_latAwal") String latwal,
            @Field("f_lngAwal") String lattujuan,
            @Field("f_awal") String awal ,
            @Field("f_latAkhir") String latakhir,
            @Field("f_lngAkhir") String lonakhir ,
            @Field("f_akhir") String akhir ,
            @Field("f_jarak") String jarak ,
            @Field("f_tarif") String tarif


    );




    @FormUrlEncoded
    @POST("get_booking")
    Call<ResponseHistory> request_history(
            @Field("f_idUser") String iduser,
            @Field("status") String status
    );


    //tambhan request get posisi driver
    @FormUrlEncoded
    @POST("get_driver")
    Call<ResponseTracking> request_tracking(
            @Field("id") String iduser

    );

}
