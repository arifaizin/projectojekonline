package id.co.imastudio.ojekonlinecourse;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.imastudio.ojekonlinecourse.Helper.DirectionMapsV2;
import id.co.imastudio.ojekonlinecourse.Helper.GPSTracker;
import id.co.imastudio.ojekonlinecourse.Helper.HeroHelper;
import id.co.imastudio.ojekonlinecourse.Helper.SessionManager;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.ApiService;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.InitLibrary;
import id.co.imastudio.ojekonlinecourse.Response.Distance;
import id.co.imastudio.ojekonlinecourse.Response.Leg;
import id.co.imastudio.ojekonlinecourse.Response.OverviewPolyline;
import id.co.imastudio.ojekonlinecourse.Response.ResponseInsertBooking;
import id.co.imastudio.ojekonlinecourse.Response.ResponseRoute;
import id.co.imastudio.ojekonlinecourse.Response.Route;
import id.co.imastudio.ojekonlinecourse.View.Finddriver;
import id.co.imastudio.ojekonlinecourse.View.History;
import id.co.imastudio.ojekonlinecourse.View.login;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Build.VERSION_CODES.M;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    //@BindView(R.id.map)
    //android.widget.fragment map;
    @BindView(R.id.lokasiawal)
    TextView lokasiawal;
    @BindView(R.id.lokasitujuan)
    TextView lokasitujuan;
    @BindView(R.id.requestorder)
    Button requestorder;
    @BindView(R.id.txtharga)
    TextView txtharga;
    private GoogleMap mMap;
    String jarakkm ;
    Double harga;
    String name_location = "tes";
    //deklrasi variable koordinat global
    Double latawal = 0.0;
    Double lonawal = 0.0;
            Double latakhir,lonakkhir ;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if  (Build.VERSION.SDK_INT >= M
                && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    12
            );


        }
        else {

            GPSTracker gps = new GPSTracker(MainActivity.this);
            if(gps.canGetLocation()){

                latawal = gps.getLatitude() ;
                lonawal = gps.getLongitude() ;

                Log.d(TAG, "onCreate: lokasi"+ latawal+lonawal);

//                String name =posisiku(latawal,lonawal);
                name_location = posisiku(latawal,lonawal);

                Log.d(TAG, "onCreate: nama lokasi"+ name_location);
//                LatLng l = new LatLng(latawal,lonawal);
//
//                mMap.addMarker(new MarkerOptions().position(l).title(name_location));
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(l,14));
//
//                lokasiawal.setText(name_location);

            }
            else{
                gps.showSettingGps();
            }


        }


    }
    private String posisiku(Double latawal, Double lonawal) {
        name_location = null;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> list = geocoder.getFromLocation(latawal, lonawal, 1);
            name_location = list.get(0).getAddressLine(0) + "" + list.get(0).getCountryName();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return name_location;}

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

        LatLng l = new LatLng(latawal,lonawal);

        mMap.addMarker(new MarkerOptions().position(l).title(name_location));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(l,14));

        lokasiawal.setText(name_location);
//        LatLng sydney = new LatLng(6.1925297, -106.8001397);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        // menampilkan control zoom in zoom out
        mMap.getUiSettings().setZoomControlsEnabled(true);
        // menampilkan compas
        mMap.getUiSettings().setCompassEnabled(true);
        // mengatur jenis peta
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.history:

                startActivity(new Intent(MainActivity.this, History.class));


                break;

            case R.id.Profil:
                //untuk tombol profile nanti bakal muncul informasi dari user yang login
                //yang bakal muncul nanti nama user,email dan hp nya
                //

                final SessionManager sesi = new SessionManager(MainActivity.this);

                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Profil User");
                alert.setMessage("Nama :"+ sesi.getNama()+"\n"+
                                    "Email :" +sesi.getEmail()+ "\n"+
                                    "Handphone :" + sesi.getPhone()
                );
                alert.setPositiveButton("Sign Out", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       //event saat button sign out di klik


                        sesi.logout();
                        Intent intent = new Intent(MainActivity.this,login.class);
                        startActivity(intent);
                        finish();

                    }
                });
                //untuk manampilkan dialog
                alert.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.lokasiawal, R.id.lokasitujuan,R.id.requestorder})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //
            case R.id.lokasiawal:
                // aksi disini dijalanka
                completeAuto(1);
                break;
            case R.id.lokasitujuan:
                completeAuto(2);
                // disini dijalankan
                break;
            
            case R.id.requestorder :
                
                actionInsertServer();
                
                break;
        }
    }

    private void actionInsertServer() {
        ApiService api = InitLibrary.getInstance();

        //https://pastebin.com/jgeYiNrQ

        SessionManager sesi = new SessionManager(MainActivity.this);

        //get token dan id dari sessionmanager yang kita udah simpan waktu login
        String token = sesi.getToken();

        HeroHelper.pre("token :"+token);
        String iduser = sesi.getIdUser() ;
        //get nama lokasi dari textview
        String lokasi1 = lokasiawal.getText().toString() ;
        String lokasi2 = lokasitujuan.getText().toString() ;


        //get koordinat sebelum double kita ubah jadi string
        String lat1 = String.valueOf(latawal);
        String lon1 = String.valueOf(lonawal);
        String lat2 = String.valueOf(latakhir);
        String lon2 = String.valueOf(lonakkhir);

        //get harga dan jarak dari hasil route
        //untuk konversi type data dari double ke string String.valueOf
        String price = String.valueOf(harga) ;
        String jarak = jarakkm;

        String device = HeroHelper.getDeviceId(MainActivity.this);


        //get request
        Call<ResponseInsertBooking> call = api.request_insertbooking(token,device,iduser,
                lat1,lon1,lokasi1,lat2,lon2,lokasi2,jarak,price);

        //get response
        call.enqueue(new Callback<ResponseInsertBooking>() {
            @Override
            public void onResponse(Call<ResponseInsertBooking> call, Response<ResponseInsertBooking> response) {
                //response succes
//                if(response.isSuccessful()){
                    String result = response.body().getResult();
                    String message = response.body().getMsg();
                    //get id booking kalau seandainya booking itu berhasil masuk database
                    String idbooking = response.body().getIdBooking();
                    String waktu = response.body().getWaktu();

//                    Toast.makeText(MainActivity.this, "success: "+ result+message+idbooking+waktu, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: insert"+ result+message+idbooking);


                    //check response bernilai true /false
                    if(result.equals("true")){

                        //pindah halaman ke halaman lain
                        Intent intent = new Intent(MainActivity.this,Finddriver.class);
                        intent.putExtra("id",idbooking);
                        startActivity(intent);

                    }
                    else{
                        //bkin toast kalau seandai hasil resultnya nggak true
                        HeroHelper.pesan(MainActivity.this,message);

                    }

//                }
//                else{
//                    HeroHelper.pesan(MainActivity.this,"Not Success :" +response.toString());
//                }
            }

            @Override
            public void onFailure(Call<ResponseInsertBooking> call, Throwable t) {
                Toast.makeText(MainActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void completeAuto(int i) {
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("ID") // set filter negara
                .build();

        //
        Intent intent = null;
        try {
            intent = new PlaceAutocomplete
                    .IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(typeFilter)
                    .build(MainActivity.this);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, i);

    }

    //ini sebuah method untuk ambil data nama lokasi yang di klik oleh pengguna di autocomplete
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode != 0) {
            //get data
            Place place = PlaceAutocomplete.getPlace(MainActivity.this, data);
            //get coordinat
            latawal= place.getLatLng().latitude;
            lonawal = place.getLatLng().longitude;
            //masukkan k latlang biar bisa di masukkan k maps
            LatLng awal = new LatLng(latawal, lonawal);

            mMap.clear();

            //buat marker berbdasarkan koordinat daptkan atas
            mMap.addMarker(new MarkerOptions()
                    .position(awal).title(place.getAddress().toString()));
            lokasiawal.setMaxLines(1);
            lokasiawal.setText(place.getAddress());


            //set auto zoom
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(awal, 16));


        } else if (requestCode == 2) {
            Place place = PlaceAutocomplete.getPlace(MainActivity.this, data);
            //get coordinat

            if (place != null){
                latakhir = place.getLatLng().latitude;
                lonakkhir = place.getLatLng().longitude;
                //masukkan k latlang biar bisa di masukkan k maps
                LatLng akhir = new LatLng(latakhir, lonakkhir);

                //buat marker berbdasarkan koordinat daptkan atas
                mMap.addMarker(new MarkerOptions()
                        .position(akhir).title(place.getAddress().toString()));
                lokasitujuan.setMaxLines(1);
                lokasitujuan.setText(place.getAddress());


                //set auto zoom
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(akhir, 16));

                actionRoute();            }


        }
    }

    private void actionRoute() {
        //get init retrofit yang sudah dibkin class inilibrary
        ApiService api = InitLibrary.getInstance2();

        String origin = String.valueOf(latawal) +","+ String.valueOf(lonawal);
        String destination = String.valueOf(latakhir) +","+String.valueOf(lonakkhir);

        //get request
        Call<ResponseRoute> call = api.request_route(origin,
                destination, getString(R.string.google_maps_key));

        //get response
        call.enqueue(new Callback<ResponseRoute>() {
            @Override
            public void onResponse(Call<ResponseRoute> call, Response<ResponseRoute> response) {

//                String name = "nando";
//                String[] name2 = {"nando","setpain","husn"};

                //response succes
                if (response.isSuccessful()) {

                    if (response.body().getStatus().equals("OK")) {


                        //get route from server
                        List<Route> route = response.body().getRoutes();
                        //get object overview polyline

                        Log.d(TAG, "onResponse: " + route);

                        Route object = route.get(0);
                        OverviewPolyline overview = object.getOverviewPolyline();
                        String point = overview.getPoints();
                        DirectionMapsV2 direction = new DirectionMapsV2(MainActivity.this);
                        direction.gambarRoute(mMap, point);

                        //ambil jarak
                        List<Leg> legs = object.getLegs();
                        Leg object0 = legs.get(0);
                        //get distance
                        Distance jarak = object0.getDistance();
                        jarakkm = jarak.getText();
                        long jarakmeter = jarak.getValue();

                        //jarak meter kita jadikan km
                        long km = jarakmeter / 1000;

                        //jadikan string
                        String km2 = String.valueOf(km);

                        //pembulatan nilainya biar gmpang di kalculasi
                        //sample : 15640 jadi 16000
                        Double totaljarak = Math.ceil(Double.parseDouble(km2));

                        //ini sebuah kondisional harga
                        //kalau seandainya jarak kurang dari 5 km total harga nya 4000
                        //dan kalau melebihi dari 5 km permeter melebihi 5 di kalikan 2000 + harga normal (4000)


                        if (totaljarak >= 5) {
                            //total harga
                            harga = (totaljarak - 5) * 2000 + 4000;

                        } else {
                            harga = 4000.0;
                        }


                        //format rupiah
                        //sample 10000 kalau pake format 10.000.00

                        txtharga.setText("Rp." + HeroHelper.toRupiahFormat2(String.valueOf(harga)) + "(" + jarakkm + ")");


                    } else {
                        Toast.makeText(MainActivity.this, ""+ response.body().getStatus(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(MainActivity.this, "Response Not Success : "+ response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseRoute> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Response Failure : "+ t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }


}
