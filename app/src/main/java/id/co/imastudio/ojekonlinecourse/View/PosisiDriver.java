package id.co.imastudio.ojekonlinecourse.View;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.ApiService;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.InitLibrary;
import id.co.imastudio.ojekonlinecourse.MainActivity;
import id.co.imastudio.ojekonlinecourse.R;
import id.co.imastudio.ojekonlinecourse.Response.Datum2;
import id.co.imastudio.ojekonlinecourse.Response.ResponseTracking;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PosisiDriver extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.lokasiawal)
    TextView lokasiawal;
    @BindView(R.id.lokasitujuan)
    TextView lokasitujuan;
    @BindView(R.id.txtnamadriver)
    TextView txtnamadriver;
    @BindView(R.id.txthpdriver)
    TextView txthpdriver;
    String id ;
    GoogleMap mMap ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posisi_driver);
        ButterKnife.bind(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //get data berdasarkan yang kirim oleh class finddriver
        String lokasiawall = getIntent().getStringExtra("lokasi1");
        String lokasiakhir = getIntent().getStringExtra("lokasi2");
        id = getIntent().getStringExtra("driver");


        //tambahan di di webinar untuk ambil posisi driver yang tak booking penggunanya
        //ok ?
        getDriver();

        //pindahin data ke textview
        lokasiawal.setText(lokasiawall);
        lokasitujuan.setText(lokasiakhir);

    }


    //method untuk setting maps
    //sample: setting zoom,type maps ,etc
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap ;
    }

    private void getDriver() {

        ApiService api = InitLibrary.getInstance();

        //saat request
        Call<ResponseTracking> call = api.request_tracking(id);

        call.enqueue(new Callback<ResponseTracking>() {
            @Override
            public void onResponse(Call<ResponseTracking> call, Response<ResponseTracking> response) {


                //ini saat server kasi data tracking driver
                if(response.isSuccessful()){

                    List<Datum2> data = response.body().getData();
                    //get latlng from driver
                    String lat  = data.get(0).getTrackingLat();
                    String lon = data.get(0).getTrackingLng();

                    //coordinat ubah nilainya ke double
                    //convert type data dari string menuju double
                    //kenapa double ?
                    //karena kalau type datanya string coordinat nggak masuk map android

                    Double lat1 = Double.parseDouble(lat);
                    Double lon1 = Double.parseDouble(lon);

                    //pindahkan ke maps
                    LatLng posisi = new LatLng(lat1,lon1);

                    //kita buat sebuah marker berdasarkan dari coordinat yang kirim oleh server
                    mMap.addMarker(new MarkerOptions().position(posisi).title("driver"));
                    //animatecamera untuk marker driver posisinya di bagian tengah
                    //atau kata bpak di tengah layar .
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(posisi,12));

                    txtnamadriver.setText(data.get(0).getTrackingDriver());
                    txthpdriver.setText(data.get(0).getUserHP());

                }
            }

            @Override
            public void onFailure(Call<ResponseTracking> call, Throwable t) {

            }
        });



    }


    Timer autoUpdate ;
    public void onResume() {
        super.onResume();
        autoUpdate = new Timer();
        autoUpdate.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {

                        getDriver();


                        //RbHelper.pesan(c,"ngulang");


                    }
                });
            }
        }, 0, 3000); // updates each 40 secs
    }

    @Override
    protected void onPause() {
        autoUpdate.cancel();
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_BACK){

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Where you go ?");
            alert.setPositiveButton("Exit App", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });

            alert.setNegativeButton("Home", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(new Intent(PosisiDriver.this, MainActivity.class));
                    finish();
                }
            });
            alert.show();

        }


        return super.onKeyDown(keyCode, event);
    }
}
