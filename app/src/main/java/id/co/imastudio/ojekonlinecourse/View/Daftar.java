package id.co.imastudio.ojekonlinecourse.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.imastudio.ojekonlinecourse.Helper.HeroHelper;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.ApiService;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.InitLibrary;
import id.co.imastudio.ojekonlinecourse.R;
import id.co.imastudio.ojekonlinecourse.Response.ResponseDaftar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Daftar extends AppCompatActivity {

    @BindView(R.id.daftarusername)
    EditText daftarusername;
    @BindView(R.id.daftaremail)
    EditText daftaremail;
    @BindView(R.id.daftarhp)
    EditText daftarhp;
    @BindView(R.id.daftarpassword)
    EditText daftarpassword;
    @BindView(R.id.daftarconfirmasipass)
    EditText daftarconfirmasipass;
    @BindView(R.id.btnsignup)
    Button btnsignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        ButterKnife.bind(this);


    }

    @OnClick(R.id.btnsignup)
    public void onViewClicked() {
        /*membuat sebuah kondisional untuk memastikan kalau pengguna
        sudah pengisi sebuah textfield yang ada
         */

       //get semua inputan semua user
        String tampungnama = daftarusername.getText().toString();
        String tampungEmail = daftaremail.getText().toString() ;
        String tampunghp = daftarhp.getText().toString() ;
        String tampungpass = daftarpassword.getText().toString() ;
        String tampungconfirm = daftarconfirmasipass.getText().toString();

        if(!tampungpass.equals(tampungconfirm)){
            Toast.makeText(Daftar.this,"passsword tidak cocok",Toast.LENGTH_SHORT).show();

        }


       else if(!tampungnama.isEmpty() && !tampungEmail.isEmpty() && !tampunghp.isEmpty() &&
                !tampungpass.isEmpty() && !tampungconfirm.isEmpty()){

            //get init retrofit yang sudah dibkin class inilibrary
            ApiService api = InitLibrary.getInstance();

            //get request
            Call<ResponseDaftar> call = api.request_daftar(tampungnama,tampungEmail,
                    tampungpass,tampunghp);

            //get response
            call.enqueue(new Callback<ResponseDaftar>() {
                @Override
                public void onResponse(Call<ResponseDaftar> call, Response<ResponseDaftar> response) {

                    //response succes
                    if(response.isSuccessful()){
                        String result = response.body().getResult();
                        String message = response.body().getMsg();
                        //check response bernilai true /false
                        if(result.equals("1")){

                            //pindah halaman ke halaman lain
                            Intent intent = new Intent(Daftar.this,login.class);
                            startActivity(intent);

                        }
                        else{
                            //bkin toast kalau seandai hasil resultnya nggak true
                            HeroHelper.pesan(Daftar.this,message);

                        }

                    }
                    else{

                    }
                }

                @Override
                public void onFailure(Call<ResponseDaftar> call, Throwable t) {

                }
            });







        }
        else{
            //kalau seandainya ada yang kosong kita bkin message box
            Toast.makeText(Daftar.this,"maaf,harus diisi semua",Toast.LENGTH_SHORT).show();



        }





    }
}
