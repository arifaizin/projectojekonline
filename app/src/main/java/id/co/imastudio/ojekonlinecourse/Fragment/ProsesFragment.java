package id.co.imastudio.ojekonlinecourse.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.co.imastudio.ojekonlinecourse.Helper.CustomRecycler;
import id.co.imastudio.ojekonlinecourse.Helper.HeroHelper;
import id.co.imastudio.ojekonlinecourse.Helper.SessionManager;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.ApiService;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.InitLibrary;
import id.co.imastudio.ojekonlinecourse.R;
import id.co.imastudio.ojekonlinecourse.Response.Datum;
import id.co.imastudio.ojekonlinecourse.Response.ResponseHistory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProsesFragment extends Fragment {


    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    Unbinder unbinder;

    public ProsesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_proses, container, false);
        unbinder = ButterKnife.bind(this, view);

        getDataHistory();
        return view;
    }

    private void getDataHistory() {
        ApiService api = InitLibrary.getInstance();

        SessionManager sesi = new SessionManager(getActivity());

        //get request
        Call<ResponseHistory> call = api.request_history(sesi.getIdUser(), "1");

        //get response
        call.enqueue(new Callback<ResponseHistory>() {
            @Override
            public void onResponse(Call<ResponseHistory> call, Response<ResponseHistory> response) {

                //response succes
                if (response.isSuccessful()) {
                    String result = response.body().getResult();
                    String message = response.body().getMsg();
                    //check response bernilai true /false
                    if (result.equals("true")) {

                        //get data history user
                        List<Datum> data = response.body().getData();
                        //pindahkan data yang sudah di dapatkan dari server k recyclerview

                        CustomRecycler adapter = new CustomRecycler(data, getActivity());
                        recyclerview.setAdapter(adapter);
                        LinearLayoutManager linear = new LinearLayoutManager(getActivity());
                        recyclerview.setLayoutManager(linear);


                    } else {
                        //bkin toast kalau seandai hasil resultnya nggak true
                        HeroHelper.pesan(getActivity(), message);

                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseHistory> call, Throwable t) {

            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
