package id.co.imastudio.ojekonlinecourse.InitRetrofit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nandoseptianhusni on 8/28/17.
 */

public class InitLibrary {


    //inizialisasi sebuah library untuk accesss data dari server
    //library yang digunakan adalah retrofit dari square

    static OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();

    public static Retrofit setInit() {

        return new Retrofit.Builder().baseUrl("https://jekojek.000webhostapp.com/ojeg_server/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

    }public static Retrofit setInit2() {

        return new Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/directions/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

    }

    public static ApiService getInstance(){

        return setInit().create(ApiService.class);


    }public static ApiService getInstance2(){

        return setInit2().create(ApiService.class);


    }




}


