package id.co.imastudio.ojekonlinecourse.View;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.imastudio.ojekonlinecourse.Helper.HeroHelper;
import id.co.imastudio.ojekonlinecourse.Helper.SessionManager;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.ApiService;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.InitLibrary;
import id.co.imastudio.ojekonlinecourse.MainActivity;
import id.co.imastudio.ojekonlinecourse.R;
import id.co.imastudio.ojekonlinecourse.Response.Data;
import id.co.imastudio.ojekonlinecourse.Response.ResponseLogin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends AppCompatActivity {

    @BindView(R.id.loginemail)
    EditText loginemail;
    @BindView(R.id.loginpassword)
    EditText loginpassword;
    @BindView(R.id.signin)
    Button signin;
    @BindView(R.id.textlink)
    TextView textlink;
    //deklrasi variable untuk class sesi
    SessionManager sesi ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //inisialisasi untuk alias sesi
        sesi = new SessionManager(login.this);
    }

    @OnClick({R.id.signin, R.id.textlink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.signin:
                
                actionLogin();
                
                break;
            case R.id.textlink:

                Intent intent = new Intent(login.this,Daftar.class);
                startActivity(intent);
                break;
        }
    }

    private void actionLogin() {
          /*membuat sebuah kondisional untuk memastikan kalau pengguna
        sudah pengisi sebuah textfield yang ada
         */
        if  (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION},
                    12
            );


        }
        else {
        //get semua inputan semua user
        String tampungdevice = HeroHelper.getDeviceId(login.this);
        final String tampungEmail = loginemail.getText().toString() ;

        String tampungpass = loginpassword.getText().toString() ;
        

        


        if(!tampungEmail.isEmpty()  && !tampungpass.isEmpty() ){




                //get init retrofit yang sudah dibkin class inilibrary
                ApiService api = InitLibrary.getInstance();

                //get request
                Call<ResponseLogin> call = api.request_login(tampungdevice, tampungEmail, tampungpass);

                //get response
                call.enqueue(new Callback<ResponseLogin>() {
                    @Override
                    public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {

                        //response succes
                        if (response.isSuccessful()) {
                            String result = response.body().getResult();
                            String message = response.body().getMsg();

                            //get data user yang login
                            //untuk apa ? biar nanti data user yang login d simpan chace
                            //nanti kita bkin kondisional kalau data nggak da berrti dia belum login

                            //biar user login tidak berulang -ulang


                            //check response bernilai true /false
                            if (result.equals("true")) {

                                //get token
                                String token = response.body().getToken();
                                //get class data untuk ambil semua informasi user yang login
                                Data data = response.body().getData();
                                String tampungiduser = data.getIdUser();
                                String tampungnamauser = data.getUserNama();
                                String tampungemail2 = data.getUserEmail();
                                String tampunghp = data.getUserHp();

                                //kita masukkan class sessionmanager biar kesimpan ke chace

                                sesi.cerateLoginSession(token);
                                sesi.setIduser(tampungiduser);
                                sesi.setEmail(tampungemail2);
                                sesi.setNama(tampungnamauser);
                                sesi.setPhone(tampunghp);

                                //pindah halaman ke halaman lain
                                Intent intent = new Intent(login.this, MainActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                //bkin toast kalau seandai hasil resultnya nggak true
                                HeroHelper.pesan(login.this, message);

                            }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseLogin> call, Throwable t) {
                        Log.d("error login", t.getMessage());


                    }
                });





        }
        else{
            //kalau seandainya ada yang kosong kita bkin message box
            Toast.makeText(login.this,"maaf,harus diisi semua",Toast.LENGTH_SHORT).show();



        }



    }}


}
