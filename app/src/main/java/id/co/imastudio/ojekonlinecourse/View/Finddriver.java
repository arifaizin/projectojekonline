package id.co.imastudio.ojekonlinecourse.View;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.imastudio.ojekonlinecourse.Helper.HeroHelper;
import id.co.imastudio.ojekonlinecourse.Helper.SessionManager;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.ApiService;
import id.co.imastudio.ojekonlinecourse.InitRetrofit.InitLibrary;
import id.co.imastudio.ojekonlinecourse.MainActivity;
import id.co.imastudio.ojekonlinecourse.R;
import id.co.imastudio.ojekonlinecourse.Response.Datum;
import id.co.imastudio.ojekonlinecourse.Response.ResponseDaftar;
import id.co.imastudio.ojekonlinecourse.Response.ResponseHistory;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Finddriver extends AppCompatActivity {

    @BindView(R.id.pulsator)
    PulsatorLayout pulsator;
    @BindView(R.id.buttoncancel)
    Button buttoncancel;
    String id ;

    private static final String TAG = "Finddriver";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finddriver);
        ButterKnife.bind(this);

       id = getIntent().getStringExtra("id");
        Log.d(TAG, "Booking ID : "+ id);

        pulsator.start();

        getstatusBooking();
        
    }

    private void getstatusBooking() {
        ApiService api = InitLibrary.getInstance();

        SessionManager sesi  = new SessionManager(Finddriver.this);

        //get request
        Call<ResponseHistory> call = api.request_check(
                //nilai id kita convert ke string karena id di api service bertype data string
                String.valueOf(id),
                //get token from sessionManager
                sesi.getToken(),
                //get device id
                HeroHelper.getDeviceId(Finddriver.this));

        //get response
        call.enqueue(new Callback<ResponseHistory>() {
            @Override
            public void onResponse(Call<ResponseHistory> call, Response<ResponseHistory> response) {

                //response succes
                if(response.isSuccessful()){
                    String result = response.body().getResult();
                    String message = response.body().getMsg();

                    //check response bernilai true /false
                    if(result.equals("true")){

                        //get data history user
                        List<Datum> data = response.body().getData();
                        Intent i = new Intent(Finddriver.this,PosisiDriver.class);
                        //angkut data booking dan driver ke posisi driver
                            //get lokasi awal

                        //kenapa kita disini menggunkaan get(0) karena data array hanya ada satu data
                        //ingat ! index array di mulai dari 0
                        //kalau data array cuman satu bertti di mulai dari 0 bukan 1
                        String lokasiawal = data.get(0).getBookingFrom();
                        String lokasitujuan = data.get(0).getBookingTujuan();
                        //saat booking udah d take sama driver
                        //id driver kita pindahkan ke posisi driver
                        String iddriver = String.valueOf(data.get(0).getBookingDriver());

                        //kita pindah kan data
                        i.putExtra("lokasi1",lokasiawal);
                        i.putExtra("lokasi2",lokasitujuan);
                        i.putExtra("driver",iddriver);



                        startActivity(i);
                        //pindahkan data yang sudah di dapatkan dari server k recyclerview





                    }
                    else{
                        //bkin toast kalau seandai hasil resultnya nggak true
//                        HeroHelper.pesan(Finddriver.this,message);
                        Log.d(TAG, "onResponse: result false");

                    }

                }
                else{

                }
            }

            @Override
            public void onFailure(Call<ResponseHistory> call, Throwable t) {

            }
        });


    }

    @OnClick(R.id.buttoncancel)
    public void onViewClicked() {

        AlertDialog.Builder alert  = new AlertDialog.Builder(Finddriver.this);
        alert.setMessage("Anda Yakin untuk cancel orderan ini ?");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setPositiveButton("YA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                actionCancel();
            }
        });
        alert.setNegativeButton("tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    private void actionCancel() {

        ApiService api = InitLibrary.getInstance();

        SessionManager sesi  = new SessionManager(Finddriver.this);

        //get request
        Call<ResponseDaftar> call = api.request_cancel(String.valueOf(id),sesi.getToken(),
                HeroHelper.getDeviceId(Finddriver.this));

        //get response
        call.enqueue(new Callback<ResponseDaftar>() {
            @Override
            public void onResponse(Call<ResponseDaftar> call, Response<ResponseDaftar> response) {

                //response succes
                if(response.isSuccessful()){
                    String result = response.body().getResult();
                    String message = response.body().getMsg();
                    //check response bernilai true /false
                    if(result.equals("true")){

                        //pindah halaman ke halaman lain
                        Intent intent = new Intent(Finddriver.this,MainActivity.class);

                        startActivity(intent);

                        finish();

                    }
                    else{
                        //bkin toast kalau seandai hasil resultnya nggak true
                        HeroHelper.pesan(Finddriver.this,message);

                    }

                }
                else{

                }
            }

            @Override
            public void onFailure(Call<ResponseDaftar> call, Throwable t) {

            }
        });





    }
    Timer  autoUpdate ;
    public void onResume() {
        super.onResume();
        autoUpdate = new Timer();
        autoUpdate.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {

                        getstatusBooking();
                        //RbHelper.pesan(c,"ngulang");


                    }
                });
            }
        }, 0, 3000); // updates each 40 secs
    }

    @Override
    protected void onPause() {
        autoUpdate.cancel();
        super.onPause();
    }
}
